# SVG Placeholder Images

## Using It Locally

Clone this repo and use your local web server of choice to serve the `/public` folder.

The copy button will only be given access to your keyboard if your local server runs on `localhost` or `127.0.0.1`.
