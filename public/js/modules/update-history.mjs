/**
 * Update History
 */

'use strict';

/**
 * Update History
 * 
 * @param {FormData} formData
 */
export default function updateHistory( formData ) {
	const searchParams = new URLSearchParams( formData );

	window.history.replaceState(
		null,
		document.title,
		window.location.origin + '/?' + searchParams.toString()
	);
}
