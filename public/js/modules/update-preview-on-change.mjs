'use strict';

/**
 * Main
 * 
 * @param {HTMLElement} previewElement
 */
export default function main( previewElement ) {
	window.addEventListener( 'output-updated', onOutputUpdated.bind( null, previewElement ) );
}

/**
 * On Output Updated
 * 
 * @param {HTMLElement} previewElement
 * @param {UIEvent} event
 */
function onOutputUpdated( previewElement, event ) {
	if ( null === previewElement.firstChild ) {
		previewElement.appendChild( event.detail.svg );
	} else {
		previewElement.replaceChild( event.detail.svg, preview.firstChild );
	}
}

