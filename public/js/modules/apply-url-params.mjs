/**
 * Apply URL Params
 */

'use strict';

/**
 * Apply URL Params
 * 
 * @param {HTMLFormElement} form
 * @param {string} searchString
 */
export default function applyUrlParams( form, searchString ) {

	if ( '' === searchString ) {
		return;
	}

	const allowList = [
		'inline-size',
		'block-size',
		'background-color',
		'text-color',
		'format'
	];

	const params = new URLSearchParams( searchString );

	params.forEach( function( value, key ) {
		if ( -1 === allowList.indexOf( key ) ) {
			return;
		}

		form.elements.namedItem( key ).value = value
	} );

}
