/**
 * Resize
 */

'use strict';

/**
 * Main
 * 
 * @param {HTMLTextAreaElement}
 */
export default function main( element ) {
	if ( ! hasBrowserSupport() ) {
		return;
	}
	window.addEventListener( 'output-updated', resize.bind( null, element ) );
}

/**
 * Get Padding
 *
 * @param {HTMLTextAreaElement} element 
 * @returns 
 */
function getPadding( element ) {
	const padding = window.getComputedStyle( element ).getPropertyValue( 'padding-block-start' ).replace( 'px', '' );
	return isNaN( parseFloat( padding ) ) ? 0 : parseFloat( padding );
}

/**
 * Resize
 *
 * @param {HTMLTextAreaElement} element
 */
function resize( element ) {
	element.style.blockSize = 'auto';
	element.style.blockSize = ( element.scrollHeight + ( getPadding( element ) * 2 ) ) + 'px';
}

/**
 * Has Browser Support
 *
 * @return bool
 */
function hasBrowserSupport() {
	return 'getComputedStyle' in window;
}
