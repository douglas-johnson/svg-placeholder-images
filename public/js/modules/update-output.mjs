/**
 * Update Output
 */

'use strict';

/**
 * Hex Regex
 */
const hexRegex = new RegExp( '^#[a-fA-F0-9]{3,6}$' );

/**
 * Update Ouput
 * 
 * @param {FormData} formData 
 */
export default function updateOutput( formData ) {

	const format    = formData.get( 'format' );
	const width     = getValidNumber( formData.get( 'inline-size' ), 100 );
	const height    = getValidNumber( formData.get( 'block-size' ), 100 );
	const fill      = getValidHexColor( formData.get( 'background-color' ), '#000000' );
	const textColor = getValidHexColor( formData.get( 'text-color' ), '#EEEEEE' );

	const svg = document.createElementNS( 'http://www.w3.org/2000/svg', 'svg' );

	svg.setAttribute( 'width', width );
	svg.setAttribute( 'height', height );
	svg.setAttribute( 'viewBox', `0 0 ${width} ${height}` );
	svg.setAttribute( 'xmlns', 'http://www.w3.org/2000/svg' );

	const rect = document.createElementNS( 'http://www.w3.org/2000/svg', 'rect' );

	rect.setAttribute( 'width', width  );
	rect.setAttribute( 'height', height );
	rect.setAttribute( 'fill', fill );

	const text = document.createElementNS( 'http://www.w3.org/2000/svg', 'text' );

	text.setAttribute( 'fill', textColor );
	text.setAttribute( 'font-family', 'sans-serif'  );
	text.setAttribute( 'x', '50%'  );
	text.setAttribute( 'y', '50%'  );
	text.setAttribute( 'text-anchor', 'middle' );

	text.textContent = `${width}x${height}`;

	text.setAttribute( 'font-size', ( Math.min( width, height ) / text.textContent.length ).toFixed( 0 ) );

	svg.appendChild( rect );
	svg.appendChild( text );

	if ( 'img' === format ) {
		output.textContent = forImg( encodeSVG( svg.outerHTML ), text.textContent, width, height );
	} else if ( 'css' === format ) {
		output.textContent = forCSS( encodeSVG( svg.outerHTML ) );
	} else if ( 'css-pseudo' === format ) {
		output.textContent = forCSSPseudo( encodeSVG( svg.outerHTML ) );
	} else {
		output.textContent = svg.outerHTML;
	}

	window.dispatchEvent(
		new CustomEvent(
			'output-updated',
			{
				detail: {
					format,
					svg,
					width,
					height
				}
			}
		)
	);

}

/**
 * Get Valid Number
 *
 * @param value
 * @param {number} defaultValue
 * @returns
 */
function getValidNumber( value, defaultValue ) {
	if ( isNaN( parseInt( value ) ) ) {
		return defaultValue;
	}
	return parseInt( value );
}

/**
 * Get Valid Hex Color
 *
 * @param value
 * @param {string} defaultValue
 * @return {string}
 */
function getValidHexColor( value, defaultValue ) {
	if ( true !== hexRegex.test( value ) ) {
		return defaultValue;
	}
	return value;
}
 
/**
 * Img
 *
 * @param {string} encodedSVG
 * @param {string} alt
 * @param {number} width
 * @param {number} height
 * @return {string}
 */
function forImg( encodedSVG, alt, width, height ) {
	return `<img src="data:image/svg+xml;charset=UTF-8,${encodedSVG}" width="${width}" height="${height}" alt="${alt} Placeholder Image">`;
}

/**
 * CSS
 *
 * @param {string} encodedSVG
 * @return {string}
 */
function forCSS( encodedSVG ) {
	return `background-image: url( 'data:image/svg+xml;charset=UTF-8,${encodedSVG}' );`;
}

/**
 * CSS Pseudo
 * 
 * @param {string}
 * @return {string}
 */
function forCSSPseudo( encodedSVG ) {
	return `content: url( 'data:image/svg+xml;charset=UTF-8,${encodedSVG}' );`;
}

/**
 * Encode SVG
 * 
 * @param {string} svgHTML
 * @return {string}
 */
function encodeSVG( svgHTML ) {
	return encodeURIComponent(
		svgHTML
			// Strip newlines and tabs
			.replace(/[\t\n\r]/gim, '')
			// Condense multiple spaces
			.replace(/\s\s+/g, ' ')
	)
	// Encode parenthesis
	.replace(/\(/g, '%28')
	.replace(/\)/g, '%29');
}
