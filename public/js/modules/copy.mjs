/**
 * Copy Button
 */

'use strict';

/**
 * Main
 */
export default function main() {
	
	const button = document.getElementById( 'clipboard-button' );
	const output = document.getElementById( 'output' );
	
	if ( ! hasBrowserSupport() ) {
		return;
	}

	button.setAttribute( 'aria-hidden', false );
	button.setAttribute( 'tabindex', '0' );

	addEvents( button, output );
}

/**
 * Has Browser Support
 *
 * @return bool
 */
function hasBrowserSupport() {
	return ( 'navigator' in window ) && ( 'clipboard' in window.navigator );
}

/**
 * Add Events
 *
 * @param {HTMLElement} button
 * @param {HTMLElement} output
 */
function addEvents( button, output ) {
	button.addEventListener( 'click', onClick.bind( null, output ) );
}

/**
 * On Click
 *
 * @param {HTMLElement} output
 * @param {UIEvent} event
 */
function onClick( output, event ) {
	window.navigator.clipboard.writeText( output.textContent ).then( function(){
		event.target.classList.add( 'is-loaded' );
		setTimeout( clearButtonClasses.bind( null, event.target ), 1000 );
	} );
}

/**
 * Clear Button Classes
 *
 * @param {HTMLElement} button
 */
function clearButtonClasses( button ) {
	button.classList.remove( 'is-loaded' );
}
