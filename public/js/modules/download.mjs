/**
 * Download
 */

'use strict';

/**
 * Main
 */
export default function main() {
	
	const link = document.getElementById( 'download-link' );
	
	if ( ! hasBrowserSupport() ) {
		return;
	}

	window.addEventListener( 'output-updated', onOutputUpdated.bind( null, link ) );
}

/**
 * 
 * @param {HTMLElement} link 
 * @param {CustomEvent} event 
 */
function onOutputUpdated( link, event ) {
	if ( 'svg' === event.detail.format ) {
		updateDownload( link, event.detail );
	} else {
		disableDownload( link );
	}
}

/**
 * Update Download
 *
 * @param {HTMLElement} link
 * @param {Object} detail
 */
function updateDownload( link, detail ) {
	link.href = URL.createObjectURL(
		new Blob( [detail.svg.outerHTML], { type: 'image/svg+xml' } )
	);
	link.download = `${detail.width}x${detail.height}-placeholder.svg`;
	link.setAttribute( 'aria-hidden', false );
	link.setAttribute( 'tabindex', '0' );
}

/**
 * Disable Download
 *
 * @param {HTMLElement} link
 */
function disableDownload( link ) {
	link.removeAttribute( 'href' );
	link.removeAttribute( 'download' );
	link.setAttribute( 'aria-hidden', true );
	link.setAttribute( 'tabindex', '-1' );
}

/**
 * Has Browser Support
 * 
 * @return {boolean} Browser supports Blob and URL constructor.
 */
function hasBrowserSupport() {
	return ( 'Blob' in window ) && ( 'URL' in window );
}
