/**
 * SVG Placeholder Images
 */

'use strict';

import applyUrlParams from './modules/apply-url-params.mjs';
import copy from './modules/copy.mjs';
import download from './modules/download.mjs';
import resize from './modules/resize.mjs';
import updateHistory from './modules/update-history.mjs';
import updateOutput from './modules/update-output.mjs';
import updatePreviewOnChange from './modules/update-preview-on-change.mjs';

const input    = document.getElementById( 'input' );
const output   = document.getElementById( 'output' );
const preview  = document.getElementById( 'preview' );

/**
 * Init Copy Button
 */
copy();

/**
 * Init Download Button
 */
download();

/**
 * Init Resize Events
 * Resize the output textarea based on output-updated event.
 */
resize( output );

/**
 * Init Preview Events
 * Update preview based on output-updated event.
 */
updatePreviewOnChange( preview );

/**
 * Apply URL params to the input form.
 */
applyUrlParams( input, window.location.search );

/**
 * Update output based on current state of input form.
 */
updateOutput( new FormData( input ) );

/**
 * Watch for changes to the form input.
 */
input.addEventListener( 'change', function( event ) {
	const formData = new FormData( event.target.form );
	updateHistory( formData );
	updateOutput( formData );
} );
